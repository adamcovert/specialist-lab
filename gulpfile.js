'use strict';

const pjson = require('./package.json');
const dirs = pjson.config.directories;

const gulp = require('gulp');
const less = require('gulp-less');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const replace = require('gulp-replace');
const fileinclude = require('gulp-file-include');
const del = require('del');
const browserSync = require('browser-sync').create();
const ghPages = require('gulp-gh-pages');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const cheerio = require('gulp-cheerio');
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const base64 = require('gulp-base64');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const cleanCSS = require('gulp-cleancss');
const size = require('gulp-size');
const spritesmith = require('gulp.spritesmith');
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');
const sorting = require('postcss-sorting');

// Less
gulp.task('less', function(){
  return gulp.src(dirs.source + '/less/style.less')
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(rename('style.css'))
    .pipe(postcss([
      autoprefixer({ browsers: ['last 4 version'] }),
      sorting({
        'properties-order': [
          'position',
          'content',
          'display',
          'flex-wrap',
          'fles-direction',
          'align-items',
          'justify-content',
          'top',
          'bottom',
          'left',
          'right',
          'width',
          'max-width',
          'height',
          'max-height',
          'object-fit',
          'object-position',
          'background',
          'background-color',
          'background-image',
          'background-repeat',
          'background-size',
          'background-position',
          'color',
          'margin',
          'margin-top',
          'margin-right',
          'margin-bottom',
          'margin-left',
          'padding',
          'padding-top',
          'padding-right',
          'padding-bottom',
          'padding-left',
          'border',
          'border-radius',
          'font-family',
          'font-size',
          'font-style',
          'font-weight',
          'font-variant',
          'letter-spacing',
          'line-height',
          'list-style-type',
          'text-align',
          'text-decoration',
          'text-transform',
          'vertical-align',
          'cursor',
          'touch-action',
          'user-select',
          'transition',
          'animation-name',
          'animation-duration',
          'animation-delay',
          'animation-timing-function',
          'animation-fill-mode',
          'transform',
          'opacity',
          'overflow',
        ]
      }),
      mqpacker({ sort: true }),
    ]))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest(dirs.build + '/css/'))
    .pipe(rename('style.min.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest(dirs.build + '/css/'))
    .pipe(browserSync.stream());
});

// Html
gulp.task('html', function() {
  return gulp.src(dirs.source + '/*.html')
    .pipe(plumber({ errorHandler: onError }))
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file',
      indent: true,
    }))
    .pipe(replace(/\n\s*<!--DEV[\s\S]+?-->/gm, ''))
    .pipe(gulp.dest(dirs.build));
});

// Img
gulp.task('img', function () {
  return gulp.src([
        dirs.source + '/img/*.{gif,png,jpg,jpeg,svg}',
      ],
      {since: gulp.lastRun('img')}
    )
    .pipe(plumber({ errorHandler: onError }))
    .pipe(newer(dirs.build + '/img'))
    .pipe(gulp.dest(dirs.build + '/img'));
});

// Video copying
gulp.task('video:copy', function () {
  console.log('---------- Videos copying');
  return gulp.src(dirs.source + '/video/*.{mp4,ogv,webm}', {since: gulp.lastRun('video:copy')})
    .pipe(newer(dirs.build + '/video'))
    .pipe(size({
      title: 'Размер',
      showFiles: true,
      showTotal: false,
    }))
    .pipe(gulp.dest(dirs.build + '/video'));
});

// Fonts copying
gulp.task('fonts:copy', function () {
  console.log('---------- Fonts copying');
  return gulp.src(dirs.source + '/fonts/*.{ttf,woff,woff2,eot,svg}', {since: gulp.lastRun('fonts:copy')})
    .pipe(newer(dirs.build + '/fonts'))
    .pipe(size({
      title: 'Размер',
      showFiles: true,
      showTotal: false,
    }))
    .pipe(gulp.dest(dirs.build + '/fonts'));
});

// Img opt.
gulp.task('img:opt', function () {
  return gulp.src([
      dirs.source + '/img/*.{gif,png,jpg,jpeg,svg}',
      '!' + dirs.source + '/img/sprite-svg.svg',
    ])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(dirs.source + '/img'));
});

// Svg sprite
gulp.task('svgstore', function (callback) {
  let spritePath = dirs.source + '/img/svg-sprite';
  if(fileExist(spritePath) !== false) {
    return gulp.src(spritePath + '/*.svg')
      // .pipe(plumber({ errorHandler: onError }))
      .pipe(svgmin(function (file) {
        return {
          plugins: [{
            cleanupIDs: {
              minify: true
            }
          }]
        }
      }))
      .pipe(svgstore({ inlineSvg: true }))
      .pipe(cheerio(function ($) {
        $('svg').attr('style',  'display:none');
      }))
      .pipe(rename('sprite-svg.svg'))
      .pipe(gulp.dest(dirs.source + '/img'));
  }
  else {
    console.log('Нет файлов для сборки SVG-спрайта');
    callback();
  }
});

// Png Sprite
gulp.task('png:sprite', function () {
  let fileName = 'sprite-' + Math.random().toString().replace(/[^0-9]/g, '') + '.png';
  let spriteData = gulp.src('src/img/png-sprite/*.png')
    .pipe(plumber({ errorHandler: onError }))
    .pipe(spritesmith({
      imgName: fileName,
      cssName: 'sprite.less',
      cssFormat: 'less',
      padding: 4,
      imgPath: '../img/' + fileName
    }));
  let imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest('build/img'));
  let cssStream = spriteData.css
    .pipe(gulp.dest(dirs.source + '/less/'));
  return merge(imgStream, cssStream);
});

gulp.task('clean', function () {
  return del([
    dirs.build + '/**/*',
    '!' + dirs.build + '/readme.md'
  ]);
});

// Javascript
gulp.task('js', function () {
  return gulp.src([
      dirs.source + '/js/jquery-3.2.1.min.js',
      dirs.source + '/js/owl.carousel.min.js',
      dirs.source + '/js/dropdown.js',
      dirs.source + '/js/modal.js',
      dirs.source + '/js/collapse.js',
      dirs.source + '/js/transition.js',
      dirs.source + '/js/script.js',
    ])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dirs.build + '/js'));
});

// Build
gulp.task('build', gulp.series(
  'clean',
  'svgstore',
  'png:sprite',
  gulp.parallel('less', 'img', 'js', 'fonts:copy'),
  'html'
));


gulp.task('serve', gulp.series('build', function() {

  browserSync.init({
    //server: dirs.build,
    server: {
      baseDir: "./build/"
    },
    port: 3000,
    startPath: '/index.html',
    // open: false
  });

  gulp.watch(
    [
      dirs.source + '/*.html',
      dirs.source + '/_include/*.html',
    ],
    gulp.series('html', reloader)
  );

  gulp.watch(
    dirs.source + '/less/**/*.less',
    gulp.series('less', reloader)
  );

  gulp.watch(
    dirs.source + '/img/svg-sprite/*.svg',
    gulp.series('svgstore', 'html', reloader)
  );

  gulp.watch(
    dirs.source + '/img/*.{gif,png,jpg,jpeg,svg}',
    gulp.series('img', reloader)
  );

  gulp.watch(
    dirs.source + '/js/*.js',
    gulp.series('js', reloader)
  );

  gulp.watch(
    dirs.source + '/img/png-sprite/*.png',
    gulp.series('png:sprite', 'less', reloader)
  );

}));

gulp.task('deploy', function() {
  return gulp.src('./build/**/*')
    .pipe(ghPages());
});

gulp.task('default',
  gulp.series('serve')
);

function reloader(done) {
  browserSync.reload();
  done();
}

function fileExist(path) {
  const fs = require('fs');
  try {
    fs.statSync(path);
  } catch(err) {
    return !(err && err.code === 'ENOENT');
  }
}

var onError = function(err) {
    notify.onError({
      title: "Error in " + err.plugin,
    })(err);
    this.emit('end');
};
