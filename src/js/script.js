$(document).ready(function(){

  (function($) {
    $('.question-answer__accordion > li:eq(0) a').addClass('active').next().slideDown();
    $('.question-answer__accordion a').click(function(j) {
      var dropDown = $(this).closest('li').find('p');
      $(this).closest('.question-answer__accordion').find('p').not(dropDown).slideUp();
      if ($(this).hasClass('active')) {
          $(this).removeClass('active');
      } else {
        $(this).closest('.question-answer__accordion').find('a.active').removeClass('active');
        $(this).addClass('active');
      }
      dropDown.stop(false, true).slideToggle();
      j.preventDefault();
    });
  })(jQuery);

});

// Google map
var mapPoints = [
  [
    'SRT-Parts',
    59.906689,
    30.268278
  ],
];

function initMap() {
  var mapDiv = document.getElementById('map');
  var center = {lat: 59.906689, lng: 30.268278};
  var map = new google.maps.Map(mapDiv, {
    zoom: 14,
    center: center,
    scrollwheel: false,
    styles: [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#c9c9c9"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#84accb"
          },
          {
            "visibility": "on"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }
    ]
  });

  setMapMarkers(map);

  map.addListener('resize', function() {
    map.panTo(center);
  });
  google.maps.event.addDomListener(window, 'resize', function(){
    google.maps.event.trigger(map, 'resize');
  });
}

function setMapMarkers(map) {
  var icon = {
    url: '../img/map-point.png',
    fillColor: '#1c0579',
    fillOpacity: 0.9,
    scale: 0.6,
    strokeColor: '#1c0579',
    strokeWeight: 1,
    size: new google.maps.Size(44, 78),
    anchor: new google.maps.Point(22, 67)
  };

  for (var i = 0; i < mapPoints.length; i++) {
    var point = mapPoints[i];
    var marker = new google.maps.Marker({
      position: {lat: point[1], lng: point[2]},
      map: map,
      icon: icon,
      title: point[0],
      html: point[3],
    });
  }
}